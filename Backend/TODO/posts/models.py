from importlib.resources import contents
from django.db.models import *
from django.utils.timezone import now

# Create your models here.
class Author(Model):
    id = IntegerField(primary_key = True)

    nickname = CharField(
        verbose_name = "Имя",
        max_length = 100
    )

    registration_date = DateTimeField(
        verbose_name = "Дата регестрации",
        default = now,
        null = True,
        blank = True
    )
    
    description = CharField(
        verbose_name = "О себе",
        max_length = 500
    )

    adult = BooleanField(
        verbose_name = "Взрослый"
    )

    def get_records(self) -> QuerySet:
        return Record.objects.filter(author__id = self.id)

    def __str__(self) -> str:
        return self.nickname

class Record(Model):
    id = IntegerField(primary_key = True)

    author = ForeignKey(
        to = "Author",
        on_delete = CASCADE
    )

    content = TextField(
        verbose_name = "Содержание"
    )

    is_nsfw = BooleanField(
        verbose_name = "is_nsfw"
    )

    is_edited = BooleanField(
        verbose_name = "измененный",
        default = False
    )

    time_create = DateTimeField(
        verbose_name = "Дата создания записи",
        default = now,
        null = True,
        blank = True
    )

    def get_tags(self) -> QuerySet:
        return RecordTag.objects.filter(record__id = self.id)

    def add_tag(self, name: str) -> None:
        if len(name) > 100:
            raise ValueError("nickname is too long")

        new_tag = RecordTag(record = self, tag_name = name)
        new_tag.save()

    def __str__(self) -> str:
        return f"{self.id}:{self.author.nickname}:{self.time_create}"

class RecordTag(Model):
    id = IntegerField(primary_key = True)

    record = ForeignKey(
        to = "Record",
        on_delete = CASCADE
    )

    tag_name = CharField(
        max_length = 100
    )

    def __str__(self) -> str:
        return f"{self.id}:{self.tag_name}"
